#include "Renderer.hpp"
#include "network_packages.hpp"
#include <cstdint>

#ifdef __linux__
#include <unistd.h>
#define sleep(x) usleep(x*1000)
#elif _WIN32
#include <windows.h>
#define sleep(x) Sleep(x)
#endif

Renderer::Renderer(Dimensions d, ServerResponse* response, ClientInput* clientinput)
    : response(response),
      clientinput(clientinput),
      mWindow(sf::VideoMode(d.screen.x, d.screen.y), "Snek"),
      mWidth(d.screen.x),
      mHeight(d.screen.y),
      mSnakeBlockSize(d.snake_block_size)
{}

Renderer::~Renderer()
{}

void Renderer::handleEvents()
{
    // Process events
    sf::Event event;
    while (mWindow.pollEvent(event))
    {
        // Close window: exit
        if (event.type == sf::Event::Closed)
            mWindow.close();
        if (event.type == sf::Event::KeyPressed){
            if (event.key.code == sf::Keyboard::Space) (*clientinput).restart = true;
            if (event.key.code == sf::Keyboard::Left)  (*clientinput).input = {-1,  0};
            if (event.key.code == sf::Keyboard::Right) (*clientinput).input = { 1,  0};
            if (event.key.code == sf::Keyboard::Up)    (*clientinput).input = { 0, -1};
            if (event.key.code == sf::Keyboard::Down)  (*clientinput).input = { 0,  1};
        }
    }
}

void Renderer::draw()
{
    sf::RectangleShape snake_shape({(float)mSnakeBlockSize,(float)mSnakeBlockSize});
    sf::RectangleShape apple_shape({(float)mSnakeBlockSize,(float)mSnakeBlockSize});

    // Clear screen
    mWindow.clear();

    //std::vector<coordinates> snake = (*response).SnakeLocation;
    for(unsigned int i = 1; i < snake.size(); i++){
        snake_shape.setPosition({
                (float)(snake[i].x * mSnakeBlockSize),
                (float)(snake[i].y * mSnakeBlockSize)
                });
        if (i == 0)
            snake_shape.setFillColor({0,255,0});
        else
            snake_shape.setFillColor({255,255,255});
        mWindow.draw((snake_shape));
    }
    snake_shape.setFillColor({0,255,0});
    if(snake.size() >= 1){
        snake_shape.setPosition({
                (float)(snake[0].x * mSnakeBlockSize),
                (float)(snake[0].y * mSnakeBlockSize)
                });
        mWindow.draw((snake_shape));
    }

    auto apple = (*response).AppleLocation;
    apple_shape.setPosition({
            (float)(apple.x * mSnakeBlockSize),
            (float)(apple.y * mSnakeBlockSize)
            });
    apple_shape.setFillColor({255,0,0});
    mWindow.draw(apple_shape);

    // Update the window
    mWindow.display();
}

void Renderer::drawLoop()
{
    mClock.restart();
    handleEvents();
    
    draw();
    mFrameTime = mClock.getElapsedTime().asSeconds();
    float to_sleep = (1.0f/mTargetFPS - mFrameTime) * 1000;
    if (to_sleep > 0) sleep(to_sleep);
}
