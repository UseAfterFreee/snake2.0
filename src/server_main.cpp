#include <SFML/Network.hpp>
//#include <Socket.hpp>
#include <iostream>
#include "Engine.hpp"
#include "Renderer.hpp"
#include "network_packages.hpp"
#ifdef SERVER_MAIN
int main()
{   
    /*
    // Create the main window
    const int horizontal_pixels = 800;
    const int vertical_pixels = 600;
    const int snake_block_size = 40;
    const int grid_width = horizontal_pixels / snake_block_size;
    const int grid_height = vertical_pixels / snake_block_size;

    std::srand(std::time(nullptr));

    Engine engine(grid_width, grid_height);

    Renderer renderer({
        {horizontal_pixels, vertical_pixels},
        snake_block_size
    }, engine);
    renderer.drawLoop();
    */
    sf::TcpListener listener;
    if(listener.listen(42069) != sf::Socket::Done){
        // error
    }
    sf::TcpSocket client;
    if(listener.accept(client) != sf::Socket::Done){
        // error
    }
    ConnectionInit client_start;
    std::size_t received;
    // sf::Socket socket;
    if (client.receive(&client_start, sizeof(ConnectionInit), received) != sf::Socket::Done){
        // error
    }
    std::cout << "Received " << received << " bytes" << std::endl;
    std::cout << "horizontal_pixels= " << client_start.horizontal_pixels << std::endl;
    std::cout << "vertical_pixels= " << client_start.vertical_pixels << std::endl;
    std::cout << "snake_block_size= " << (unsigned)client_start.snake_block_size << std::endl;
    std::cout << "username= " << client_start.username << std::endl;


    std::srand(std::time(nullptr));
    ClientInput clientinput;
    ServerResponse response;
    uint32_t grid_width = client_start.horizontal_pixels / client_start.snake_block_size;
    uint32_t grid_height = client_start.vertical_pixels / client_start.snake_block_size;


    Engine engine(grid_width, grid_height);
    bool pause = false;
    while(69){
        if (client.receive(&clientinput, sizeof(ClientInput), received) != sf::Socket::Done) {
            //another error yeet.
        }
        if(clientinput.input != engine.getDirection() && clientinput.input != coordinates{0, 0}){
            if (clientinput.input == coordinates{-1,  0})  engine.left();
            else if (clientinput.input == coordinates{ 1,  0}) engine.right();
            else if (clientinput.input == coordinates{ 0, -1})    engine.up();
            else if (clientinput.input == coordinates{ 0,  1})  engine.down();
        }
        if(clientinput.restart){
            engine.restart();
        }
        if(clientinput.pause){
            pause = true;
        }
        if(clientinput.unpause){
            pause = false;
        }
        if(!pause && !engine.isGameOver){
            engine.runTick();
        }
        response.HeadDirection = engine.getDirection();
        response.AppleLocation = engine.getAppleLocation();
        response.SizeSnakeLocation = engine.getLocation().size();
        response.IsGameOver = engine.isGameOver;

        if(client.send(&response, sizeof(ServerResponse)) != sf::Socket::Done){
            //yeet
        }
        if(client.send(&engine.getLocation()[0], engine.getLocation().size() * sizeof(coordinates)) != sf::Socket::Done) {
            //error
        }
        std::cout << "size of snake: " <<  engine.getLocation().size() << std::endl;
        for(int i = 0; i <  engine.getLocation().size(); ++i){
            std::cout << "x: " << engine.getLocation()[i].x << "\t\ty: " << engine.getLocation()[i].y << std::endl;
        }
    }
    return 0;
}
#endif
