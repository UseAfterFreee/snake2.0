#include <vector>
#include <algorithm>
#include <cstdlib>
#include "Engine.hpp"

Engine::Engine(unsigned int width, unsigned int height)
    : mWidth(width),
      mHeight(height)
{
    restart();
}

void Engine::left() {
    if (!(mDirection == coordinates{1,0}) && !hasChangeDirection)
    {
        mDirection = {-1, 0};
        hasChangeDirection = true;
    }
}

void Engine::up() {
    if (!(mDirection == coordinates{0,1}) && !hasChangeDirection)
    {
        mDirection = {0, -1};
        hasChangeDirection = true;
    }
}

void Engine::down() {
    if (!(mDirection == coordinates{0,-1}) && !hasChangeDirection)
    {
        mDirection = {0, 1};
        hasChangeDirection = true;
    }
}

void Engine::right() {
    if (!(mDirection == coordinates{-1,0}) && !hasChangeDirection)
    {
        mDirection = {1, 0};
        hasChangeDirection = true;
    }
}

void Engine::pause() {
    mDirection = {0, 0};
}

void Engine::runTick() {
    coordinates new_location = *mSnakeLocation.begin();
    new_location = new_location + mDirection;
    hasChangeDirection = false;
    if(std::find(mSnakeLocation.begin(), mSnakeLocation.end(), new_location)
       != mSnakeLocation.end())
    {
        isGameOver = true;
        /*int* x = nullptr;
        int a = *x;*/
        pause();
    }
    if (new_location.x >= mWidth
        || new_location.x < 0
        || new_location.y >= mHeight
        || new_location.y < 0)
    {
        isGameOver = true;
        /*int* x = nullptr;
        int a = *x;*/
        pause();
    }
    else {
        if(new_location != mAppleLocation){
            mSnakeLocation.pop_back();
        } else {
            newApple();
        }
        mSnakeLocation.insert(mSnakeLocation.begin(), new_location);
    }
}

void Engine::restart()
{
    mSnakeLocation.clear();
    isGameOver = false;
    mDirection = {1, 0};
    mSnakeLocation.push_back({mWidth / 4, mHeight / 2});
    mAppleLocation = {3 * mWidth / 4, mHeight / 2};
}

void Engine::newApple(){
    bool found = false;
    while (!found)
    {
        mAppleLocation = {rand() % mWidth, rand() % mHeight};
        if (std::none_of(mSnakeLocation.begin(), mSnakeLocation.end(), [&](coordinates c){
            return c == mAppleLocation;
        })) found = true;
    }
}

std::vector<coordinates> Engine::getLocation()
{
    return mSnakeLocation;
}

coordinates Engine::getAppleLocation()
{
    return mAppleLocation;
}
coordinates Engine::getDirection(){
    return mDirection;
}
