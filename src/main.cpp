#include "Engine.hpp"
#include "Renderer.hpp"

#ifdef OLDMAIN
int main()
{
    // Create the main window
    const int horizontal_pixels = 800;
    const int vertical_pixels = 600;
    const int snake_block_size = 40;
    const int grid_width = horizontal_pixels / snake_block_size;
    const int grid_height = vertical_pixels / snake_block_size;

    std::srand(std::time(nullptr));

    Engine engine(grid_width, grid_height);

    Renderer renderer({
        {horizontal_pixels, vertical_pixels},
        snake_block_size
    }, engine);
    renderer.drawLoop();

    return 0;
}
#endif