#ifndef NETWORK_PACKAGES_HPP
#define NETWORK_PACKAGES_HPP

#include "Engine.hpp"
#include <vector>
struct ConnectionInit {
    uint32_t horizontal_pixels;
    uint32_t vertical_pixels;
    uint32_t snake_block_size;
    char username[100]; 

};
struct ConnectionInitResponse {
    uint32_t id;
};
struct ClientInput {
    bool restart;
    bool pause;
    bool unpause;
    coordinates input;
};
struct ServerResponse {
    coordinates HeadDirection;
    coordinates AppleLocation;
    uint32_t SizeSnakeLocation;
    bool IsGameOver;
};
#endif