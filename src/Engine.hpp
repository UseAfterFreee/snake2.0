#ifndef ENGINE_HPP
#define ENGINE_HPP
#include <cstdint>
#include <vector>
struct coordinates {
    int32_t x;
    int32_t y;
    coordinates operator+(coordinates other)
    {
        return {x+other.x, y+other.y};
    }
    bool operator!=(coordinates other)
    {
        return (x != other.x || y != other.y);
    }
    bool operator==(coordinates other)
    {
        return(x == other.x && y == other.y);
    }
};
class Engine
{
    private:
        std::vector<coordinates> mSnakeLocation = {{5, 5}};
        const int mWidth, mHeight;
        coordinates mAppleLocation = {2, 2};
        coordinates mDirection = {1, 0};

        void newApple();

        bool hasChangeDirection = false;
    public:
        bool isGameOver = false;

        Engine(unsigned int width, unsigned int height);

        void left();
        void up();
        void down();
        void right();
        void pause();

        // runs one tick of the engine
        void runTick();
        void restart();

        std::vector<coordinates> getLocation();
        coordinates getAppleLocation();
        coordinates getDirection();
};
#endif
