#include <SFML/Network.hpp>
#include "Engine.hpp"
#include "Renderer.hpp"
#include "network_packages.hpp"
#include <iostream>

#ifdef CLIENT_MAIN
int main()
{   
    /*
    // Create the main window
    const int horizontal_pixels = 800;
    const int vertical_pixels = 600;
    const int snake_block_size = 40;
    const int grid_width = horizontal_pixels / snake_block_size;
    const int grid_height = vertical_pixels / snake_block_size;

    std::srand(std::time(nullptr));

    Engine engine(grid_width, grid_height);

    Renderer renderer({
        {horizontal_pixels, vertical_pixels},
        snake_block_size
    }, engine);
    renderer.drawLoop();
    */
    const uint16_t horizontal_pixels = 800;
    const uint16_t vertical_pixels = 600;
    const uint8_t snake_block_size = 40;
    ConnectionInit init = {horizontal_pixels, vertical_pixels, snake_block_size, "DAB DAB DAB YEETERDIEDIE"};
    sf::TcpSocket socket;
    sf::Socket::Status status = socket.connect("localhost", 42069);
    if (status != sf::Socket::Done){
        //error
    }
    if (socket.send(&init, sizeof(ConnectionInit)) != sf::Socket::Done){
        //error
    }
    std::cout << "Sent: " << std::endl;
    std::cout << "horizontal_pixels= " << init.horizontal_pixels << std::endl;
    std::cout << "vertical_pixels= " << init.vertical_pixels << std::endl;
    std::cout << "snake_block_size= " << unsigned(init.snake_block_size) << std::endl;
    std::cout << "username= " << init.username << std::endl;

    ClientInput clientinput;
    ServerResponse response;
    std::vector<coordinates> Snakelocation;
    clientinput.restart = false;
    clientinput.pause = false;
    clientinput.unpause = true;
    Renderer renderer({
        {(int32_t)init.horizontal_pixels, (int32_t)init.vertical_pixels},
        init.snake_block_size
    }, &response, &clientinput);
    while(69){
        renderer.drawLoop();
        if(socket.send(&clientinput, sizeof(ClientInput)) != sf::Socket::Done){
            //yeet
        }
        clientinput.restart = false;
        clientinput.pause = false;
        clientinput.unpause = true;   
        std::size_t received;
        if (socket.receive(&response, sizeof(ServerResponse), received) != sf::Socket::Done) {
            //TODO: ERROR non critical
        }
        Snakelocation.resize(response.SizeSnakeLocation);
        if (socket.receive(&Snakelocation[0], response.SizeSnakeLocation * sizeof(coordinates), received) != sf::Socket::Done) {
            //error
        }
        std::cout << "Snake size: " << Snakelocation.size() << std::endl;
        for(int i = 0; i < Snakelocation.size(); ++i){
            std::cout << "x:" << Snakelocation[i].x << "\t\ty:" << Snakelocation[i].y << std::endl;
        }
        std::cout << "Package size: " << received << std::endl;
        renderer.snake = Snakelocation;
    } 
    return 0;
}
#endif
