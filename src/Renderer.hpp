#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <SFML/Graphics.hpp>
#include <cstdint>
#include "network_packages.hpp"

struct Dimensions
{
    coordinates screen;
    uint32_t snake_block_size;
};

class Renderer
{
    private:
        ServerResponse* response;
        ClientInput* clientinput;

        sf::RenderWindow mWindow;
        sf::Clock mClock;
        float mFrameTime = 1/60;

        const uint32_t mWidth;
        const uint32_t mHeight;
        const uint32_t mSnakeBlockSize;

        const uint32_t mTargetFPS = 10;

        void handleEvents();
        void draw();
    protected:
    public:
        Renderer(Dimensions d, ServerResponse* response, ClientInput* clientinput);
        ~Renderer(); // Destructor
        std::vector<coordinates> snake;
        void drawLoop();
};

#endif
